# gcalendar

A script to see the closest event in your Google Calendar on your i3blocks bar.

## Usage:

Add these lines to your i3blocks.conf.

[gcalendar]

command=/PATH_TO_SCRIPT_AND_TOKEN/gcalendar.py token.json

label=

interval=3600